#!/bin/bash
curl -O https://bkmgit.github.io/lc-shell/data/shell-lesson.zip
unzip shell-lesson.zip -d shell-lesson
cd shell-lesson
mkdir firstdir
# Comment out commands that output text without modifying any files
# cat 829-0.txt
# head 829-0.txt
# tail 829-0.txt
# less 829-0.txt
# head 829-0.txt 33504-0.txt
# head *.txt
mv 829-0.txt gulliver.txt
cp gulliver.txt gulliver-backup.txt
echo "Finally it is nice and sunny on" $(date)
history
